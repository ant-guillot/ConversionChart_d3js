## Synopsis

To explore customers and consumers path, we often try to figure out how an action is converted to value.
<br />

Thus, beeing able to understand conversion path as accurately as we can is of primary interest, this script creates a very visual and interactive view of how consumers are converted.
<br />
<img src="https://gitlab.com/ant-guillot/ConversionChart_d3js/raw/master/img/TunnelConversionExemple.png" alt="exemple" style="width: 400px;height: 300px;"/>


## Installation

1/Clone the git to your desktop
<br />
2/ Change the data with yours, they should be of the following format:
<br />
    A json with two lists:
    <br />
        -the nodes one, where each node is recorded with its name
        <br />
        -the edges name, with the source, target, value and type of the edge. There are three edges types: continue, start and end.
        <br />
3/run the index.html
        

## License

MIT license for the tunnel chart.<br />
d3js Mike Bostock MIT license for d3js.<br />